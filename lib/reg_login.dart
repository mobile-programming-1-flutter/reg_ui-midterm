import 'package:flutter/material.dart';
import 'package:reg_ui/reg_news.dart';

class REG_Login extends StatelessWidget {
  const REG_Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'BalsamiqSans',
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Colors.grey[300],
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Image(
                    image: AssetImage('assets/Buu-logo11.png'),
                    height: size.height * 0.2,
                  ),
                  Text(
                    'เข้าสู่ระบบ',
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                  Form(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person_2_outlined),
                                labelText: 'Email',
                                hintText: 'Email',
                                border: OutlineInputBorder()),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person_2_outlined),
                                labelText: 'Passwords',
                                hintText: 'Passwords',
                                border: OutlineInputBorder(),
                                suffixIcon: IconButton(
                                  onPressed: null,
                                  icon: Icon(Icons.remove_red_eye_sharp),
                                )),
                          ),
                          const SizedBox(
                            height: 2,
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              onPressed: () {},
                              child: Text('Forget Password?'),
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: () => Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (context) => const REG_News())),
                              child: Text(
                                'Login'.toUpperCase(), style: TextStyle(fontSize: 30),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
