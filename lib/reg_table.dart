import 'package:flutter/material.dart';
import 'package:reg_ui/reg_assignment.dart';
import 'package:reg_ui/reg_calendarMonth.dart';
import 'package:reg_ui/reg_login.dart';
import 'package:reg_ui/reg_news.dart';
import 'package:reg_ui/reg_userPage.dart';

class REG_Table extends StatelessWidget {
  const REG_Table({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'BalsamiqSans',
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Colors.grey[300],
          appBar: AppBar(
            title: Text(
              "ตารางเรียน",
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            backgroundColor: Colors.grey,
          ),
          drawer: const NavigationDrawer(),
          body: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text(
                  'ตารางเรียน/สอบ',
                  style: TextStyle(fontSize: 25),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: CalendarTable(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
        child: Container(
          color: Colors.grey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                buildHeader(context),
                buildMenuItem(context),
              ],
            ),
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Material(
        color: Colors.grey,
        child: InkWell(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_UserPage()));
            },
            child: Container(
              padding: EdgeInsets.only(
                top: 24 + MediaQuery.of(context).padding.top,
                bottom: 24,
              ),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 52,
                    backgroundImage:
                        NetworkImage('assets/Nile_crocodile_head.png'),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text('Pisit Karawong',
                      style: TextStyle(fontSize: 28, color: Colors.black)),
                  Text('63160209@go.buu.ac.th',
                      style: TextStyle(fontSize: 14, color: Colors.black)),
                  SizedBox(
                    height: 12,
                  ),
                ],
              ),
            )),
      );

  Widget buildMenuItem(BuildContext context) => Container(
        color: Colors.grey,
        padding: const EdgeInsets.all(10),
        child: Wrap(
          runSpacing: 1,
          children: [
            ListTile(
              leading: const Icon(Icons.newspaper),
              title: const Text(
                "ข้อมูลข่าวสาร",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_News())),
            ),
            //const Divider(color: Colors.black,),
            ListTile(
              leading: const Icon(Icons.calendar_view_week),
              title: const Text(
                "ตารางเรียน",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_Table())),
            ),
            ListTile(
              leading: const Icon(Icons.assignment),
              title: const Text(
                "ปฏิทินการศึกษา",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_CalendarMonth())),
            ),
            ListTile(
                leading: const Icon(Icons.calendar_month),
                title: const Text(
                  "ลงทะเบียน",
                  style: TextStyle(fontSize: 24, color: Colors.black),
                ),
                iconColor: Colors.black,
                onTap: () {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('ลงทะเบียนแล้ว'),
                      content: const Text(
                          'นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'OK'),
                          child: const Text(
                            'ตกลง',
                            style: TextStyle(color: Colors.green),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text(
                "ออกจากระบบ",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_Login())),
            ),
          ],
        ),
      );
}

class CalendarTable extends StatelessWidget {
  const CalendarTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 200.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth>{0: FixedColumnWidth(50)},
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        border: TableBorder.all(color: Colors.black),
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.blue[400],
            ),
            children: const <Widget>[
              ScheduleCell(
                title: 'วัน',
                isHeader: true,
              ),
              ScheduleCell(title: 'รายวิชา', isHeader: true),
              ScheduleCell(title: 'เวลา', isHeader: true),
              ScheduleCell(title: 'ห้องเรียน', isHeader: true),
            ],
          ),
          ...Schedule.getCalendarList().map((schedule) {
            return TableRow(children: [
              ScheduleCell(title: schedule.day),
              ScheduleCell(title: schedule.name),
              ScheduleCell(title: schedule.time),
              ScheduleCell(title: schedule.classroom),
            ]);
          }),
        ],
      ),
    );
  }
}

class Schedule {
  final String day;
  final String name;
  final String time;
  final String classroom;

  Schedule(this.day, this.name, this.time, this.classroom);

  static List<Schedule> getCalendarList() {
    return [
      Schedule('จันทร์', 'Software Testing', '10:00 น - 12:00 น.', 'IF-4M210'),
      Schedule('', 'Object-Oriented Analysis and Design', '13:00 น - 15:00 น.',
          'IF-3M210'),
      Schedule('', 'Web Programming', '17:00 น - 19:00 น.', 'IF-3M210'),
      Schedule('อังคาร', 'Multimedia Programming for Multiplatforms',
          '10:00 น - 12:00 น.', 'IF-4C01'),
      Schedule('', 'Object-Oriented Analysis and Design', '13:00 น - 15:00 น.',
          'IF-4C01'),
      Schedule('', 'Software Testing', '15:00 น - 17:00 น.', 'IF-3C03'),
      Schedule('พุธ', 'Mobile Application Development I', '10:00 น - 12:00 น.',
          'IF-4C01'),
      Schedule('', 'Introduction to Natural Language Processing',
          '13:00 น - 16:00 น.', 'IF-6T01'),
      Schedule('', 'Web Programming', '17:00 น - 19:00 น.', 'IF-3C01'),
      Schedule('ศุกร์', 'Multimedia Programming for Multiplatforms',
          '13:00 น - 15:00 น.', 'IF-4C01'),
      Schedule('', 'Mobile Application Development I', '15:00 น - 17:00 น.',
          'IF-3C01'),
    ];
  }
}
