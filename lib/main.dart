import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:reg_ui/reg_calendarMonth.dart';
import 'package:reg_ui/reg_login.dart';
import 'package:reg_ui/reg_news.dart';
import 'package:reg_ui/reg_table.dart';
import 'package:reg_ui/reg_userPage.dart';

void main() {
  runApp(const REG_App());
}

class REG_App extends StatelessWidget {
  const REG_App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          body: REG_Table(),
        ),
      ),
    );
  }
}