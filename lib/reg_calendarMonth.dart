import 'package:flutter/material.dart';
import 'package:reg_ui/reg_assignment.dart';
import 'package:reg_ui/reg_login.dart';
import 'package:reg_ui/reg_news.dart';
import 'package:reg_ui/reg_table.dart';
import 'package:reg_ui/reg_userPage.dart';

class REG_CalendarMonth extends StatelessWidget {
  const REG_CalendarMonth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'BalsamiqSans',
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Colors.grey[300],
          appBar: AppBar(
            title: const Text(
              "ปฏิทินการศึกษา",
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            backgroundColor: Colors.grey,
          ),
          drawer: const NavigationDrawer(),
          body: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5.0),
                
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: CalendarTable(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
        child: Container(
          color: Colors.grey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                buildHeader(context),
                buildMenuItem(context),
              ],
            ),
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Material(
        color: Colors.grey,
        child: InkWell(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_UserPage()));
            },
            child: Container(
              padding: EdgeInsets.only(
                top: 24 + MediaQuery.of(context).padding.top,
                bottom: 24,
              ),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 52,
                    backgroundImage:
                        NetworkImage('assets/Nile_crocodile_head.png'),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text('Pisit Karawong',
                      style: TextStyle(fontSize: 28, color: Colors.black)),
                  Text('63160209@go.buu.ac.th',
                      style: TextStyle(fontSize: 14, color: Colors.black)),
                  SizedBox(
                    height: 12,
                  ),
                ],
              ),
            )),
      );

  Widget buildMenuItem(BuildContext context) => Container(
        color: Colors.grey,
        padding: const EdgeInsets.all(10),
        child: Wrap(
          runSpacing: 1,
          children: [
            ListTile(
              leading: const Icon(Icons.newspaper),
              title: const Text(
                "ข้อมูลข่าวสาร",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_News())),
            ),
            //const Divider(color: Colors.black,),
            ListTile(
              leading: const Icon(Icons.calendar_view_week),
              title: const Text(
                "ตารางเรียน",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_Table())),
            ),
            ListTile(
              leading: const Icon(Icons.assignment),
              title: const Text(
                "ปฏิทินการศึกษา",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_CalendarMonth())),
            ),
            ListTile(
                leading: const Icon(Icons.calendar_month),
                title: const Text(
                  "ลงทะเบียน",
                  style: TextStyle(fontSize: 24, color: Colors.black),
                ),
                iconColor: Colors.black,
                onTap: () {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('ลงทะเบียนแล้ว'),
                      content: const Text(
                          'นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'OK'),
                          child: const Text(
                            'ตกลง',
                            style: TextStyle(color: Colors.green),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text(
                "ออกจากระบบ",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_Login())),
            ),
          ],
        ),
      );
}

class ScheduleCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const ScheduleCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.centerLeft : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}

class CalendarTable extends StatelessWidget {
  const CalendarTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth>{2: FixedColumnWidth(200)},
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.blue[400],
            ),
            children: const <Widget>[
              ScheduleCell(title: 'รายการ', isHeader: true),
              ScheduleCell(title: 'วันเริ่มต้น', isHeader: true),
              ScheduleCell(title: 'วันสุดท้าย', isHeader: true),
            ],
          ),
          ...Schedule.getCalendarList().map((schedule) {
            return TableRow(children: [
              ScheduleCell(title: schedule.lits),
              ScheduleCell(title: schedule.startDate),
              ScheduleCell(title: schedule.endDate),
            ]);
          }),
        ],
      ),
    );
  }
}

class Schedule {
  final String lits;
  final String startDate;
  final String endDate;

  Schedule(this.lits, this.startDate, this.endDate);

  static List<Schedule> getCalendarList() {
    return [
      Schedule('ช่วงจองรายวิชาลงทะเบียนให้กับนิสิตโดยเจ้าหน้าที่', '1 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      Schedule('ลงทะเบียนปกติ ( on-line )', '11 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      Schedule('ชั้นปี 4', '11 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      Schedule('ชั้นปี 3', '14 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      Schedule('ชั้นปี 2', '15 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      Schedule('ชั้นปี 1', '16 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      Schedule('ลงทะเบียนเพิ่ม/ลด/เปลี่ยนกลุ่ม', '21 พ.ย. 2565 8:30 น.', '28 พ.ย. 2565 23:59 น.'),
      Schedule('งวดชำระเงินตามปกติ', '15 ธ.ค. 2565 8:30 น.', '22 ธ.ค. 2565 23:30 น.'),
      Schedule('ช่วงชำระเงินออนไลน์หรือเคาน์เตอร์ธ.กรุงไทย ธ.ไทยพาณิชย์ ธ.กสิกร ธ.ธนชาต ธ.กรุงเทพ ทุกสาขาทั่วประเทศ', '15 ธ.ค. 2565 8:30 น.', '22 ธ.ค. 2565 23:30 น.'),
      Schedule('ช่วงวันสอบกลางภาค', '14 ม.ค. 2566 0:00 น.', '22 ม.ค. 2566 0:00 น.'),
      Schedule('ช่วงวันสอบปลายภาค', '25 มี.ค. 2566 0:00 น.', '2 เม.ย. 2566 0:00 น.'),
      Schedule('วันปิดภาคการศึกษา', '3 เม.ย. 2566 0:00 น.', '3 เม.ย. 2566 0:00 น.'),
    ];
  }
}


