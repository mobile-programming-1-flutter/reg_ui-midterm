import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:flutter_slider_drawer/flutter_slider_drawer.dart';
import 'reg_table.dart';

void main() {
  runApp(REG_Old());
}

class REG_Old extends StatefulWidget {
  const REG_Old({Key? key}) : super(key: key);

  @override
  State<REG_Old> createState() => _REG_OldState();
}

class _REG_OldState extends State<REG_Old> {
  GlobalKey<SliderDrawerState> _key = GlobalKey<SliderDrawerState>();
  final GlobalKey<SliderDrawerState> _sliderDrawerKey =
      GlobalKey<SliderDrawerState>();
  late String title;

  @override
  void initState() {
    title = "ข้อมูลข่าวสาร";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MaterialApp(
        theme: ThemeData(fontFamily: 'BalsamiqSans'),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: SliderDrawer(
              appBar: SliderAppBar(
                  appBarColor: Colors.white,
                  title: Text(title,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.w700)
                  ),
                ),
              key: _sliderDrawerKey,
              sliderOpenSize: 240,
              slider: _SliderView(
                onItemClick: (title) {
                  _sliderDrawerKey.currentState!.closeSlider();
                  setState(() {
                    this.title = title;
                  });
                },
              ),
              child: _AuthorList()),
        ),
      ),
    );
  }
}

class _SliderView extends StatelessWidget {
  final Function(String)? onItemClick;

  const _SliderView({Key? key, this.onItemClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.shade300,
      padding: const EdgeInsets.only(top: 30),
      child: ListView(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          CircleAvatar(
            radius: 80,
            backgroundColor: Colors.grey,
            child: CircleAvatar(
              radius: 80,
              backgroundImage: Image.network(
                      'https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png')
                  .image,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const Text(
            'มหาวิทยาลัยบูรพา',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 30,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          
          ...[
            Menu(Icons.newspaper, 'ข้อมูลข่าวสาร'),
            Menu(Icons.calendar_view_week, 'ตารางเรียน-สอบ'),
            Menu(Icons.app_registration, 'ลงทะเบียน'),
            Menu(Icons.assignment, 'ผลการลงทะเบียน'),
            Menu(Icons.checklist_sharp, 'ผลการอนุมัติลงทะเบียน'),
            Menu(Icons.account_circle, 'ประวัตินิสิต'),
            Menu(Icons.check, 'ตรวจสอบจบ'),
            Menu(Icons.calendar_month, 'ปฏิทินการศึกษา'),
            Menu(Icons.payment, 'ภาระค่าใช้จ่าย'),
            Menu(Icons.logout, 'ออกจากระบบ')
          ]
              .map((menu) => _SliderMenuItem(
                  title: menu.title,
                  iconData: menu.iconData,
                  onTap: onItemClick,))
              .toList(),
        ],
      ),
    );
  }
}

class _SliderMenuItem extends StatelessWidget {
  final String title;
  final IconData iconData;
  final Function(String)? onTap;

  const _SliderMenuItem(
      {Key? key,
      required this.title,
      required this.iconData,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(title,
            style: const TextStyle(
                color: Colors.black, fontFamily: 'BalsamiqSans_Regular')),
        leading: Icon(iconData, color: Colors.black),
        onTap: () => onTap?.call(title));
  }
}

class _AuthorList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Quotes> quotesList = [];
    quotesList.add(Quotes(Colors.amber, '1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565', 'ฟหกฟห'));
    quotesList.add(Quotes(Colors.orange, '1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565', '', ));
    quotesList.add(Quotes(Colors.deepOrange, '', ''));
    quotesList.add(Quotes(Colors.red, '', ''));
    quotesList.add(Quotes(Colors.purple, '', ''));
    quotesList.add(Quotes(Colors.green, '', ''));

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: ListView.separated(
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          itemBuilder: (builder, index) {
            return LimitedBox(
              maxHeight: 1000,
              child: Container(
                decoration: BoxDecoration(
                    color: quotesList[index].color,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(10.0),
                    )),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: Text(
                        quotesList[index].author,
                        style: const TextStyle(
                            fontFamily: 'BalsamiqSans_Blod',
                            fontSize: 14,
                            color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        quotesList[index].quote,
                        style: const TextStyle(
                            fontFamily: 'BalsamiqSans_Regular',
                            fontSize: 10,
                            color: Colors.black),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (builder, index) {
            return const Divider(
              height: 10,
              thickness: 0,
            );
          },
          itemCount: quotesList.length),
    );
  }
}

class Quotes {
  final MaterialColor color;
  final String author;
  final String quote;


  Quotes(this.color, this.author, this.quote);
}

class Menu {
  final IconData iconData;
  final String title;

  Menu(this.iconData, this.title,);
}

Widget buildMenuDrawer(BuildContext context) => Column(
  children: [
    ListTile(
      leading: const Icon(Icons.newspaper), 
      title: const Text("ข้อมูลข่าวสาร"),
      onTap: () {
        
      },
    )
  ],
);