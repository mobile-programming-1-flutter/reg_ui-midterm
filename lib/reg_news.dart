import 'package:flutter/material.dart';
import 'package:reg_ui/reg_assignment.dart';
import 'package:reg_ui/reg_calendarMonth.dart';
import 'package:reg_ui/reg_login.dart';
import 'package:reg_ui/reg_table.dart';
import 'package:reg_ui/reg_userPage.dart';

class REG_News extends StatelessWidget {
  const REG_News({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'BalsamiqSans',
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Colors.grey[300],
          appBar: AppBar(
            title: const Text(
              "ข้อมูลข่าวสาร",
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            backgroundColor: Colors.grey,
          ),
          drawer: const NavigationDrawer(),
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ประกาศเรื่อง',
                      style: TextStyle(
                        fontSize: 28,
                        color: Colors.black,
                      )),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                      '1.	การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565(ด่วน)',
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.black,
                      )),
                  SizedBox(
                    height: 12,
                  ),
                  Container(child: Image.asset('assets/grad652.png'),),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                      '	2.	กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565',
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.black,
                      )),
                  SizedBox(
                    height: 12,
                  ),
                  Container(child: Image.asset('assets/pay652.png'),),
                  SizedBox(
                    height: 12,
                  ),Text(
                      '3.	LINE Official',
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.black,
                      )),
                  SizedBox(
                    height: 12,
                  ),
                  Container(child: Image.asset('assets/Line.png'),),
                  SizedBox(
                    height: 12,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
        child: Container(
          color: Colors.grey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                buildHeader(context),
                buildMenuItem(context),
              ],
            ),
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Material(
        color: Colors.grey,
        child: InkWell(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_UserPage()));
            },
            child: Container(
              padding: EdgeInsets.only(
                top: 24 + MediaQuery.of(context).padding.top,
                bottom: 24,
              ),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 52,
                    backgroundImage: AssetImage('assets/Nile_crocodile_head.png'),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text('Pisit Karawong',
                      style: TextStyle(fontSize: 30, color: Colors.black)),
                  Text('63160209@go.buu.ac.th',
                      style: TextStyle(fontSize: 20, color: Colors.black)),
                  SizedBox(
                    height: 12,
                  ),
                ],
              ),
            )),
      );

  Widget buildMenuItem(BuildContext context) => Container(
        color: Colors.grey,
        padding: const EdgeInsets.all(10),
        child: Wrap(
          runSpacing: 1,
          children: [
            ListTile(
              leading: const Icon(Icons.newspaper),
              title: const Text(
                "ข้อมูลข่าวสาร",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_News())),
            ),
            //const Divider(color: Colors.black,),
            ListTile(
              leading: const Icon(Icons.calendar_view_week),
              title: const Text(
                "ตารางเรียน",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_Table())),
            ),
            ListTile(
              leading: const Icon(Icons.assignment),
              title: const Text(
                "ปฏิทินการศึกษา",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const REG_CalendarMonth())),
            ),
            ListTile(
              leading: const Icon(Icons.calendar_month),
              title: const Text(
                "ลงทะเบียน",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () {
                showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('ลงทะเบียนแล้ว'),
                  content: const Text('นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: const Text('ตกลง',style: TextStyle(color: Colors.green),),
                    ),
                  ],
                ),
              );

              }
            ),
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text(
                "ออกจากระบบ",
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              iconColor: Colors.black,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const REG_Login())),
            ),
          ],
        ),
      );
}
